package com.mq.tempDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 需要线程的方式将应用启动起来，而服务器和应用客户端需要使用 Socket 通信
 * step:2
 */
public class BrolerServer implements Runnable {
    public static int prot = 9999;
    private Socket socket;

    public BrolerServer(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        // try() 自动关闭资源，前提是 资源要实现 AutoCloseable 接口
        try(BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream()))
        {
            while(true){
                String str = in.readLine();
                if(str == null){
                    continue;
                }
                System.out.println("接收到原始数据："+str);
                if(str.equals("CONSUME")){ // 表示要消费一条消息
                    String message = Broker.consume();
                    out.println(message);
                    out.flush();
                }else{ // 其他条件都表示生产，将消息放到队列中
                    Broker.produce(str);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        ServerSocket server  = new ServerSocket(prot);
        while (true){
            BrolerServer brolerServer = new BrolerServer(server.accept());
            new Thread(brolerServer).start();
        }
    }
}
