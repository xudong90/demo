package com.mq.tempDemo;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 手动实现简单的 消息处理中心（Broker）
 * step:1
 */
public class Broker {
    // 储存消息的最大数量
    private final static int MAX_SIZE = 3;
    // 存放消息的容器，已经指定最大数量是 3
    private static ArrayBlockingQueue<String> messageQueue = new ArrayBlockingQueue<>(MAX_SIZE); // 基于数组的阻滞队列

   // 生产消息：封装了 offer()
    public static void produce(String msg){
        if(messageQueue.offer(msg)){ // offer 是插入，如果成功，就返回true，失败就返回false
            // 消费条件满足，从消息容器中取出一条消息
            System.out.println("成功向消息处理中心投送消息："+msg+",当前储存的消息数量是："+messageQueue.size());
        }else{
            System.out.println("消息处理中心的暂存消息达到最大负荷，不能继续放入消息！");
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>=================<><><><><><><>=====================<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }
    // 消费消息: 封装了 poll()
    public static String consume(){
        String msg = messageQueue.poll(); // poll 删除，如果队列为空，就返回null,否则，出队列，返回消息
        if(msg!=null){
            System.out.println("已经消费消息："+msg+",当前暂存的消息数量是："+messageQueue.size());
        }else{
            System.out.println("消息处理中心没有消息可供消费！");
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>=================<><><><><><><>=====================<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        return msg;
    }

}
