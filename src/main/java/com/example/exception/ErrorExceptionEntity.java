package com.example.exception;

/**
 * Created by Administrator on 2019/5/5.
 * 2. 异常信息模板   定义返回的异常信息的格式，这样异常信息风格更为统一
 */
public class ErrorExceptionEntity {
    private int code;
    private String message;

    public ErrorExceptionEntity() {
    }

    public ErrorExceptionEntity(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
