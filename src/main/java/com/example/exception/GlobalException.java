package com.example.exception;

/**
 * Created by Administrator on 2019/5/5.
 * 1  自定义异常
 */
public class GlobalException extends RuntimeException {
    private static final long serialVersionUID = 4564124491192825748L;
    private int code;

    public GlobalException( int code,String message) {
        super(message);
        this.code = code;
    }

    public GlobalException() {
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
