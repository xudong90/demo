package com.example.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/22.
 * 定义队列queue
 */
@Configuration
public class RabbitConfig {


    public static final String QUEUE_A = "QueueA";
    public static final String QUEUE_B = "QueueB";
    public static final String QUEUE_C = "QueueC";

    public static final String EXCHANGE_A = "exchangeA";
    public static final String EXCHANGE_B = "exchangeB";
    public static final String EXCHANGE_C = "exchangeC";

    public static final String ROUTINGKEY_A = "Routingkey_A";
    public static final String ROUTINGKEY_B = "Routingkey_B";
    public static final String ROUTINGKEY_C = "Routingkey_C";




    /**
     * 获取交换机： EXCHANGE_A
     * 针对消费者配置交换机
     * 1. 设置交换机类型
     * 2. 将队列绑定到交换机
     *  FanoutExchange: 将消息分发到所有的绑定队列，无routingkey的概念
     *  HeadersExchange ：通过添加属性key-value匹配
     *  DirectExchange:按照routingkey分发到指定队列
     *  TopicExchange:多关键字匹配
     */
    @Bean
    public DirectExchange getExchangeA() {
        return new DirectExchange(EXCHANGE_A);
    }
    /**
     * 获取队列 ：QUEUE_A
     * @return
     */
    @Bean
    public Queue queueA() {
        return new Queue(QUEUE_A, true); //第一个参数队列名称，第二个队列持久化，遇到宕机不会丢失数据
    }

    /**
     * binding:将交换机和队列通过路由关键字绑定,不绑定也可以，用默认的交换机
     * @return
     */
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queueC()).to(getExchangeC()).with(RabbitConfig.ROUTINGKEY_C);
    }
    /**
     * 获取队列 ：QUEUE_B
     * @return
     */
    @Bean
    public Queue queueB() {
        return new Queue(QUEUE_B, true); //第一个参数队列名称，第二个队列持久化，遇到宕机不会丢失数据
    }
    /**
     * 获取队列 ：QUEUE_C
     * @return
     */
    @Bean
    public Queue queueC() {
        return new Queue(QUEUE_C, true); //第一个参数队列名称，第二个队列持久化，遇到宕机不会丢失数据
    }
    @Bean
    public DirectExchange getExchangeC() {
        return new DirectExchange(EXCHANGE_C);
    }

    /**
     * =======================以下为延时队列配置==============================
     * ===============以下为延时队列配置========================================
     * ====以下为延时队列配置====================================================
     */
    public static final String QUEUE_1 = "DQUEUE_1";
    public static final String EXCHANGE_1= "EXCHANGE_1";
    public static final String ROUTING_KEY_1 = "ROUTING_KEY_1";

    public static final String QUEUE_2 = "QUEUE_2"; //延时队列TTL名称：DELAY_QUEUE_A
    public static final String EXCHANGE_2 = "EXCHANGE_2"; //DLX 死信交换机，消息到期发到这个
    public static final String ROUTING_KEY_2 = "ROUTING_KEY_2";//路由关键字


    @Bean
    public Queue Queue1(){
        return new Queue(QUEUE_1,true);
    }
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(EXCHANGE_1);
    }
    @Bean
    public Binding rBinding(){
        return BindingBuilder.bind(Queue1()).to(topicExchange()).with(ROUTING_KEY_1);
    }
    /**
     * 延迟队列配置
     * <p>
     * 1、params.put("x-message-ttl", 5 * 1000);
     * TODO 第一种方式是直接设置 Queue 延迟时间 但如果直接给队列设置过期时间,这种做法不是很灵活,（当然二者是兼容的,默认是时间小的优先）
     * 2、rabbitTemplate.convertAndSend(book, message -> {
     * message.getMessageProperties().setExpiration(2 * 1000 + "");
     * return message;
     * });
     * TODO 第二种就是每次发送消息动态设置延迟时间,这样我们可以灵活控制
     **/
    @Bean
    public Queue delayProcessQueue() {
        Map<String, Object> params = new HashMap<>();
        // x-dead-letter-exchange 声明了队列里的死信转发到的DLX名称，
        params.put("x-dead-letter-exchange", EXCHANGE_1);
        // x-dead-letter-routing-key 声明了这些死信在转发时携带的 routing-key 名称。
        params.put("x-dead-letter-routing-key", ROUTING_KEY_1);
        return new Queue(QUEUE_2, true, false, false, params);
    }

    /**
     * 死信交换机
     * @return
     */
    @Bean
    public DirectExchange delayExchange() {
        return  new DirectExchange(EXCHANGE_2);
    }
    @Bean
    public Binding dlxBinding(){
        return BindingBuilder.bind(delayProcessQueue()).to(delayExchange()).with(ROUTING_KEY_2);
    }



}
