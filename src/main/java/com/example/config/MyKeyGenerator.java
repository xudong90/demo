package com.example.config;

import com.example.model.UserModel;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@Configuration
public class MyKeyGenerator {
    @Bean("nameKeyGenerator")
    public KeyGenerator nameKeyGenerator(){
        return new KeyGenerator(){
            @Override
            public Object generate(Object o, Method method, Object... objects) {
                System.out.println(o.toString()); //Object o, 调用的类的对象  UserMapperService
                System.out.println("method.getName() is "+method.getName()); // 方法。包括整个方法的内容
                System.out.println("method.getReturnType() is "+method.getReturnType());
                System.out.println("objects is "+objects); // 方法参数
                List<Object> list = Arrays.asList(objects);
                Integer id = (Integer)list.get(0);
                return String.valueOf(id);
            }
        };
    }
    @Bean("nameKeyGenerator2")
    public KeyGenerator nameKeyGenerator2(){
        return new KeyGenerator(){
            @Override
            public Object generate(Object o, Method method, Object... objects) {
                System.out.println(o.toString()); //Object o, 调用的类的对象  UserMapperService
                System.out.println("method.getName() is "+method.getName()); // 方法。包括整个方法的内容
                System.out.println("method.getReturnType() is "+method.getReturnType());
                System.out.println("objects is "+objects); // 方法参数
                List<Object> list = Arrays.asList(objects);
                UserModel userModel = (UserModel)list.get(0);
                return String.valueOf(userModel.getId());
            }
        };
    }
}
