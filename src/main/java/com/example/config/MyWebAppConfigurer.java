package com.example.config;

import com.example.inteceptor.MyInterceptor1;
/*import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration; 这个在新版本过时了*/
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.spring.web.WebMvcRequestHandler;

//@Configuration  // 注释掉，不然swagger 起不来
public class MyWebAppConfigurer extends WebMvcConfigurationSupport {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 可以添加多个，组成拦截器链
        registry.addInterceptor(new MyInterceptor1()).addPathPatterns("/int/inteceptor");
        super.addInterceptors(registry);
    }
}
