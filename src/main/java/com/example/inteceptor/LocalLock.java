package com.example.inteceptor;

import java.lang.annotation.*;

/**
 *
 * Created by Administrator on 2019/5/5.
 * 自定义 锁的注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface LocalLock {
    String key() default "";
    /**
     * 过期时间
     * TODO 由于用的 guava 暂时就忽略这属性吧 集成 redis 需要用
     */
    int expire() default 5;
}
