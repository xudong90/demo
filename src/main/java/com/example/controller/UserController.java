package com.example.controller;

import com.example.common.JsonUtil;
import com.example.common.ResponseInfo;
import com.example.common.ResponseUtil;
import com.example.dto.UserDto;
import com.example.model.UserModel;
import com.example.service.UserMapperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

@RestController
@Api(value = "UserController" ,description = "用户控制器，测试缓存")
@RequestMapping("/user")
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(StudentController.class);
    @Autowired
    private UserMapperService userMapperService;


    @ApiOperation(value = "Cacheable根据Id查询用户信息,默认的key生产策略",notes = "id:1")
    @RequestMapping(value = "/getUserById",method = {RequestMethod.POST})
    public ResponseInfo<UserModel> getUserById(Integer id){
        log.info("===getUserById request id = "+ id);
        ResponseInfo<UserModel> responseInfo = new ResponseInfo<>();
        try{
            UserModel userModel = userMapperService.getUserById(id);
            responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
            responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
            responseInfo.setData(userModel);
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode(ResponseUtil.FAIL_CODE);
            responseInfo.setRtnMsg(ResponseUtil.FAIL_MSG);
            log.info("getUserById 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "Cacheable根据Id查询用户信息,自定义的key生产策略",notes = "userName:张三")
    @RequestMapping(value = "/getUserById2",method = {RequestMethod.POST})
    public ResponseInfo<UserModel> getUserById2(Integer id){
        log.info("=====================>getUserById2 request id = "+ id);
        ResponseInfo<UserModel> responseInfo = new ResponseInfo<>();
        try{
            UserModel userModel = userMapperService.getUserById2(id);
            responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
            responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
            responseInfo.setData(userModel);
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode(ResponseUtil.FAIL_CODE);
            responseInfo.setRtnMsg(ResponseUtil.FAIL_MSG);
            log.info("getUserByName 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "根据id删除用户信息",notes = "id:1")
    @RequestMapping(value = "/deleteUserById",method = {RequestMethod.POST})
    public ResponseInfo<UserModel> deleteUserById(Integer id){
        log.info("=================>deleteUserById request id = "+ id);
        ResponseInfo<UserModel> responseInfo = new ResponseInfo<>();
        try{
            userMapperService.deleteUserById(id);
            responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
            responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode(ResponseUtil.FAIL_CODE);
            responseInfo.setRtnMsg(ResponseUtil.FAIL_MSG);
            log.info("deleteUserById 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "CachePut 新增用户信息用户信息",notes = "userName:张三")
    @RequestMapping(value = "/inserrUser",method = {RequestMethod.POST})
    public ResponseInfo<UserModel> inserrUser(@RequestBody UserDto userDto){
        log.info("================>inserrUser request params is  = "+ JsonUtil.toJsonString(userDto));
        ResponseInfo<UserModel> responseInfo = new ResponseInfo<>();
        try{
            UserModel userModel = new UserModel();
            userModel.setUserName(userDto.getUserName());
            userModel.setEmail(userDto.getEmail());
            userModel.setGender(userDto.getGender());
            userMapperService.insertUser(userModel);
            responseInfo.setRtnCode("000");
            responseInfo.setRtnMsg("添加成功");
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode("200");
            responseInfo.setRtnMsg("添加失败");
            log.info("inserrUser 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "根据id修改用户信息",notes = "userName:张三")
    @RequestMapping(value = "/updateUserById",method = {RequestMethod.POST})
    public ResponseInfo<UserModel> updateUserById(@RequestBody UserDto userDto){
        log.info("====================>updateUserById request params is  = "+ JsonUtil.toJsonString(userDto));
        ResponseInfo<UserModel> responseInfo = new ResponseInfo<>();
        try{
            UserModel userModel = new UserModel();
            userModel.setUserName(userDto.getUserName());
            userModel.setEmail(userDto.getEmail());
            userModel.setGender(userDto.getGender());
            userModel.setId(userDto.getId());
            userMapperService.updateUserById(userModel);
            responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
            responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
            responseInfo.setData(userModel);
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode(ResponseUtil.FAIL_CODE);
            responseInfo.setRtnMsg(ResponseUtil.FAIL_MSG);
            log.info("getUserByName 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "查询所有用户数量",notes = "")
    @RequestMapping(value = "/getUserInfoCount",method = {RequestMethod.POST})
    public ResponseInfo<Integer> getUserInfoCount(){
        log.info("====================>getUserInfoCount request params is  = ");
        ResponseInfo<Integer> responseInfo = new ResponseInfo<>();
        try{
            Integer count = userMapperService.getUserInfoCount("count");
            responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
            responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
            responseInfo.setData(count);
        }catch(Exception e){
            e.printStackTrace();
            responseInfo.setRtnCode(ResponseUtil.FAIL_CODE);
            responseInfo.setRtnMsg(ResponseUtil.FAIL_MSG);
            log.info("getUserInfoCount 接口异常，错误原因："+e);
        }
        return responseInfo;
    }
    @ApiOperation(value = "rdb方式持久化，同步保存，生成dump.rdb 文件")
    @RequestMapping(value = "/saveRdb",method = {RequestMethod.POST})
    public void saveRdb(){
        Jedis jedis=new Jedis("127.0.0.1",6379);
        for (int i = 0; i <1000; i++) {
            jedis.set("key"+i, "Hello"+i);
            System.out.println("设置key"+i+"的数据到redis");
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //执行保存，会在服务器下生成一个dump.rdb数据库文件
        jedis.save();
        jedis.close();
        System.out.println("写入完成");

    }
    @ApiOperation(value = "rdb方式持久化，异步保存，生成dump.rdb 文件")
    @RequestMapping(value = "/asyncSave",method = {RequestMethod.POST})
    public void asyncSave() throws InterruptedException {
        Jedis jedis=new Jedis("127.0.0.1",6379);
        for (int i = 0; i <1000; i++) {
            jedis.set("key"+i, "Hello"+i);
            System.out.println("设置key"+i+"的数据到redis");
            Thread.sleep(2);
        }
        //执行异步保存，会在服务器下生成一个dump.rdb数据库文件
        jedis.bgsave();
        jedis.close();
        System.out.println("写入完成");

    }
    @ApiOperation(value = "根据key得到redis数据")
    @RequestMapping(value = "/getData",method = {RequestMethod.POST})
    public ResponseInfo<Object> getData(@RequestParam String key){
        Jedis jedis=new Jedis("127.0.0.1",6379);
        Object obj = jedis.get(key);
        System.out.println(obj);
        ResponseInfo responseInfo  = new ResponseInfo();
        responseInfo.setData(obj);
        responseInfo.setRtnMsg(ResponseUtil.SUCCESS_CODE);
        responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
        return responseInfo;
    }

}
