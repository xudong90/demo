package com.example.controller;

import com.example.config.RabbitConfig;
import com.example.handler.BookHandler;
import com.example.model.Student;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.AbstractJavaTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;


/**
 * Created by Administrator on 2019/3/22.
 */
@SuppressWarnings("all")
@Controller
@RequestMapping("/rabbigMq")
@Api(value="StudentRabbitMqController",description = "rabbitMq测试学生")
public class StudentRabbitMqController {
    private final RabbitTemplate rabbitTemplate;
    private static final Logger log = LoggerFactory.getLogger(StudentRabbitMqController.class);
    @Autowired
    public StudentRabbitMqController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * this.rabbitTemplate.convertAndSend(RabbitConfig.DEFAULT_BOOK_QUEUE, book); 对应 {@link BookHandler#listenerAutoAck}
     * this.rabbitTemplate.convertAndSend(RabbitConfig.MANUAL_BOOK_QUEUE, book); 对应 {@link BookHandler#listenerManualAck}
     */
    @ApiOperation(value = "rabbitmq消息队列")
    @RequestMapping(value = "/stu",method = {RequestMethod.GET})
    @ResponseBody
    public String proMsg() {
        Student stu = new Student();
        stu.setId((short)1);
        stu.setName("学习mq的旭东");
        log.info("生产者生产消息："+stu);
        this.rabbitTemplate.convertAndSend(RabbitConfig.QUEUE_B, stu);
        this.rabbitTemplate.convertAndSend(RabbitConfig.QUEUE_C, stu);
        return "success";
    }
    @ApiOperation(value = "rabbitmq延时队列")
    @RequestMapping(value = "/proDelayMsg",method = {RequestMethod.GET})
    @ResponseBody
    public String proDelayMsg() {
        Student stu = new Student();
        stu.setId((short)1);
        stu.setName("学习延时队列的旭东");
        stu.setAddress("上海宝山");
        log.info("生产者生产  延时  消息："+stu);
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_2,RabbitConfig.ROUTING_KEY_2 ,stu,message ->{
            // TODO 第一句是可要可不要,根据自己需要自行处理
            message.getMessageProperties().setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, Student.class.getName());
            // TODO 如果配置了 params.put("x-message-ttl", 5 * 1000); 那么这一句也可以省略,具体根据业务需要是声明 Queue 的时候就指定好延迟时间还是在发送自己控制时间
            message.getMessageProperties().setExpiration(5 * 1000 + "");
            return message;
        } );
        log.info("[发送时间] - [{}]", LocalDateTime.now());
        return "success";
    }
}
