package com.example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value="InteceptorController",description = "拦截器和过滤器示例")
@RequestMapping("int")
public class InteceptorController {
    @RequestMapping(value = "/filter",method = RequestMethod.GET)
    @ApiOperation("过滤器")
    public String filter(){
        System.out.println("========> 过滤器一枚飘过！");
        return "过滤器飘过";
    }
    @RequestMapping(value = "/inteceptor",method = RequestMethod.GET)
    @ApiOperation("拦截器")
    public String inteceptor(){
        System.out.println("========> 拦截器一枚飘过！");
        return "拦截器飘过";
    }
}
