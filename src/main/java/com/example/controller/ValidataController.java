package com.example.controller;

import com.example.dto.ValidataBookDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * Created by Administrator on 2019/5/5.
 * 轻松搞定数据验证
 */
@Validated
@RestController
@RequestMapping("/validata")
@Api(value = "ValidataController",description = "一些数据校验,还有问题没有成功")
public class ValidataController {
    @GetMapping("/test2")
    public String test2(  String name) {
        return "success";
    }

    @GetMapping("/test3")
    public String test3(@Validated ValidataBookDto validataBookDto) {
        return "success";
    }
}
