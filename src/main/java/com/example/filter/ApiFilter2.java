package com.example.filter;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
//@SpringBootApplication
// @WebFilter(urlPatterns = "/int/filter",filterName = "api2") // servlet 3.0 提供的不是 sprig的
public class ApiFilter2 implements Filter { // 执行顺序根据 类名字母倒叙排列
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long start = System.currentTimeMillis();
        System.out.println("刚刚进入注解过滤器，时间："+start);
        filterChain.doFilter(servletRequest,servletResponse);
        long end = System.currentTimeMillis();
        System.out.println("注解过滤器执行完方法了，时间："+end);
        System.out.println("注解过滤器和方法用时："+(end-start));
    }

    @Override
    public void destroy() {

    }
}
