package com.example.filter;


import javax.servlet.*;
import java.io.IOException;
import java.util.logging.LogRecord;

public class ApiFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        long start = System.currentTimeMillis();
        System.out.println("刚刚进入过滤器，时间："+start);
        filterChain.doFilter(servletRequest,servletResponse);
        long end = System.currentTimeMillis();
        System.out.println("过滤器执行完方法了，时间："+end);
        System.out.println("过滤器和方法用时："+(end-start));
    }

    @Override
    public void destroy() {

    }
}
