package com.example.dao;

import com.example.model.UserModel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface UserMapper {
    @Select("select * from user_info where id=#{id}")
    UserModel getUserById(Integer id);
    @Select("select * from user_info where userName=#{userName}")
    UserModel getUserByName(String userName);
    @Insert("insert into user_info values (id_seq.nextval,#{userName},#{email},#{gender})")
    void insertUser(UserModel userModel);
    @Update("update user_info set userName=#{userName},email=#{email},gender=#{gender} where id=#{id}")
    void updateUserById(UserModel userModel);
    @Delete("delete from user_info where id=#{id}")
    void deleteUserById(Integer id);
    @Select("select id_seq.currval from dual")
    int getCurrentIdSeq();
    @Select("select count(*) from user_info")
    int getCount();
}
