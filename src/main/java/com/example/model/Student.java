package com.example.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/3/15.
 * 最后实现序列化，不然很多地方可能有问题
 */
public class Student implements Serializable{
    private Short id;
    private String name;
    private Short age;
    private String address;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Student(Short id, String name, Short age, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public Student() {
    }
    public String toString(){
        return "[id:"+this.id+",age:"+this.age+",name:"+this.name+",address:"+this.address+"]";
    }

}
