package com.example;

import com.example.model.Student;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;

public class demo {
    private KieSession statefulKieSession = null;
    static String font = "dd";
    public static void main(String[] args) {
        try {
                        // load up the knowledge base
                        KnowledgeBase kbase = readKnowledgeBase();
                        StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
                   //     KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newFileLogger(ksession, "test");
                        // go !
                        Student student = new Student();
                        student.setAge((short)13);
                         ksession.insert(student);
                         ksession.fireAllRules();
                         ksession.dispose();
                         System.out.println(student);
                    //     logger.close();
                     } catch (Throwable t) {
                         t.printStackTrace();
                     }
    }
    private static KnowledgeBase readKnowledgeBase() throws Exception {
                KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
                 kbuilder.add(ResourceFactory.newClassPathResource("age.drl"), ResourceType.DRL);
                 KnowledgeBuilderErrors errors = kbuilder.getErrors();
                 if (errors.size() > 0) {
                         for (KnowledgeBuilderError error: errors) {
                                System.err.println(error);
                             }
                        throw new IllegalArgumentException("Could not parse knowledge.");
                     }
                 KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
                 kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
                return kbase;
             }
}
