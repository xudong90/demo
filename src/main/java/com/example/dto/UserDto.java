package com.example.dto;

import io.swagger.annotations.ApiModelProperty;

public class UserDto {
    @ApiModelProperty(value = "用户名",required = true)
    private String userName;
    @ApiModelProperty(value = "电子邮件",required = true)
    private String email;
    @ApiModelProperty(value = "性别",required = true)
    private Integer gender;
    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
}
