package com.example.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Administrator on 2019/3/19.
 */
public class StuByIdScopeDto {
    @ApiModelProperty(value="最小id",required = true)
    private int Id1;
    @ApiModelProperty(value="最大id",required = true)
    private int Id2;

    public int getId1() {
        return Id1;
    }

    public void setId1(int id1) {
        Id1 = id1;
    }

    public int getId2() {
        return Id2;
    }

    public void setId2(int id2) {
        Id2 = id2;
    }
}
