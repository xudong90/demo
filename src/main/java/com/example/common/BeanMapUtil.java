package com.example.common;


import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;

import java.util.Map;

/**
 * Created by Administrator on 2019/3/22.
 */
public class BeanMapUtil {
    public static Map<?,?> objectToMap(Object obj){
        if(obj==null) return null;
        return new BeanMap(obj);
    }
    public static Object mapToObject(Map<String,Object> map,Class<?> beanClass) throws Exception{
        if(map == null) return null;
        Object obj = beanClass.newInstance();
        BeanUtils.populate(obj,map);
        return obj;
    }
}
