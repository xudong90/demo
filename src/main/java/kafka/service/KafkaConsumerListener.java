package kafka.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.kafka.listener.MessageListener;

public class KafkaConsumerListener implements MessageListener <String,String>{
    @Override
    public void onMessage(ConsumerRecord<String, String> record) {
        System.out.println("[ partition = "+record.partition()+",offset = "+record.offset()+",key = "+record.key()+",value = "+record.value());

    }
}
