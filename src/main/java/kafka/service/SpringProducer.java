package kafka.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;

public class SpringProducer {
    public static void main(String[] args) {
        produce();
    }
    public static void produce(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-kafka.xml");
        KafkaTemplate<String,String> kafkaTemplate = (KafkaTemplate)ac.getBean("kafkaTemplate");
        kafkaTemplate.send("kafka_topic",0,"key1","我的测试消息1");
        kafkaTemplate.send("kafka_topic","我的测试消息2");
        kafkaTemplate.send("kafka_topic","我的测试消息3");
    }
}
