package kafka;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

public class ConsumerSample {
    public static void main(String[] args) {
        consumeMsg();
    }
    public static void consumeMsg(){
        String topic = "test-topic";
        Properties props = new Properties();
        // 和生产者一样，表示Kafka集群，如果多台服务器，用逗号隔开
        props.put("bootstrap.servers","localhost:9092");
        // group.id 表示消费者的分组 Id
        props.put("group.id","testGroup1");
        // Consumer 的 offset 是否自动提交
        props.put("enable.auto.commit","true");
        // 自动提交offset 到zookeeper 的时间间隔，单位毫秒
        props.put("auto.commit.interval.ms","10000");
        // ey.serializer 和 value.serializer： 消息反序列化类型
        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        /**
         * 接下来是消费者使用 subscribe方法订阅了topic 的消息，可以定好多，
         * poll 方法轮询Kafka 集群消息，一直等到集群中没有消息或到达超时时间，实例是100毫秒，
         */
        Consumer<String,String> consumer = new KafkaConsumer<String, String>(props);
        consumer.subscribe(Arrays.asList(topic));
        while (true){
            ConsumerRecords<String, String> records = consumer.poll(10000);
            for (ConsumerRecord <String, String> record : records) {
                System.out.println("[ partition = "+record.partition()+",offset = "+record.offset()+",key = "+record.key()+",value = "+record.value());

            }

        }
    }
}
