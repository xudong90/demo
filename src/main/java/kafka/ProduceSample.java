package kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;


public class ProduceSample {
    private static Logger logger = Logger.getLogger(ProduceSample.class);
    public static void main(String[] args) {
        productMsg();
    }
    public static void productMsg(){
        Map<String,Object> props = new HashMap<>();
        //bootstrap.servers: kafka集群，如果有多台物理服务器，就用逗号隔开
        props.put("bootstrap.servers","localhost:9092");
        //key.serializer 和 value.serializer： 消息序列化类型 ，
        // kafka 是以键值对形式发送消息到服务器的，在发送消息前生产者需要把不同类型的消息序列化为二进制类型，示例是文本类型，用的String
        props.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        // ey.serializer 和 value.serializer： 消息反序列化类型
        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        // zk.connect 用于指定 连接 zookeeper 的url ,提供了基于zookeeper 的集群服务器自动感知能力，可以动态从zookeeper获取kafka集群配置信息
        props.put("zk.connect","127.0.0.1:2181");

        String topic = "test-topic";
        Producer<String,String> producer = new KafkaProducer<String, String>(props);
        /**
         有了生产者之后就可以调用send方法发送消息了，该方法的入参数 ProducerRecord 类型对象，该类型常见构造形参3种
         ProducerRecord(topic,partition,key,value)
         ProducerRecord(topic,key,value)
         ProducerRecord(topic,value)
         topic 和value 是不能省略的，如果指定了 partition ，消息会发到指定的分区，
         如果制定了 key,没有指定 分区，会按照hash(key) 发送到指定的分区；
         如果只有topic 和value,会按照 round-robin 模式，即轮询的方式，发到没一个 partition 分区
         */

        producer.send(new ProducerRecord<>(topic,"idea-key2","java-message 1"));
        producer.send(new ProducerRecord<>(topic,"idea-key2","java-message 2"));
        producer.send(new ProducerRecord<>(topic,"idea-key2","java-message 3"));

        producer.close();
    }

}
