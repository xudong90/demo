package kafka.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kafka.ConsumerSample;
import kafka.ProduceSample;
import kafka.listener.ConsumerLinstener;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/kafka")
@Api(value="KafkaController",description = "kafka消息队列演示")
public class KafkaController {
    private static Logger logger = Logger.getLogger(ConsumerLinstener.class);

    @Autowired
    KafkaTemplate<String,String> kafkaTemplate;
    @RequestMapping(value = "produce",method = RequestMethod.POST)
    @ApiOperation(value="一般非springboot配置生产消息",notes = "")
    public void  produceMsg(){
        ProduceSample.productMsg();
    }
    @RequestMapping(value = "consume",method = RequestMethod.POST)
    @ApiOperation(value="一般非springboot配置消费消息",notes = "")
    public void consume(){
        ConsumerSample.consumeMsg();
    }


    @RequestMapping(value = "bootProduce",method = RequestMethod.POST)
    @ApiOperation(value="springboot配置生产消息",notes = "")
    public void produceBootMsg(){
        logger.info("kafka日志收集的日志。。。。。。。。");
        kafkaTemplate.send("kafka_topic",0,"key1","springboot测试消息1");
        kafkaTemplate.send("kafka_topic","我的springboot测试消息2");
        kafkaTemplate.send("kafka_topic","我的springboot测试消息3");
    }

}
